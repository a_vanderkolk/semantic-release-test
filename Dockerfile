FROM openjdk:17-slim
MAINTAINER Kolkos.nl
ARG JAR_FILE=target/*.jar
ARG DEPLOYMENTS=/deployments
ARG APP_JAR=app.jar
ENV APP_LOCATION="${DEPLOYMENTS}/${APP_JAR}"
COPY ${JAR_FILE} ${APP_LOCATION}
CMD  mkdir -p "${DEPLOYMENTS}/config"
EXPOSE 8080
ENTRYPOINT ["java","-jar","-Djava.security.egd=file:/dev/./urandom --spring.config.location=classpath:file:/deployments/config","/deployments/app.jar"]