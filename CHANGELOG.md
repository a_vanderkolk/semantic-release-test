# Semantic Versioning Changelog

# [2.4.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v2.3.0...v2.4.0) (2022-08-27)


### Features

* **sem-rel:** config ([e4b865e](https://192.168.178.21/semantic-release/semantic-release-test/commit/e4b865eb4342733aae47b461b498f640f5fc884e))

# [2.3.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v2.2.0...v2.3.0) (2022-08-27)


### Features

* **sem-rel:** config ([92acc5e](https://192.168.178.21/semantic-release/semantic-release-test/commit/92acc5edc817d1fdab4827e16a6b7c287c9a5088))

# [2.2.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v2.1.1...v2.2.0) (2022-08-27)


### Features

* **sem-rel:** config ([b233067](https://192.168.178.21/semantic-release/semantic-release-test/commit/b23306703767e541dfc8e925459080665af73e80))

## [2.1.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v2.1.0...v2.1.1) (2022-08-27)


### Bug Fixes

* **test:** test sem rel ([0980023](https://192.168.178.21/semantic-release/semantic-release-test/commit/098002314100586ee7c08594f801596613d7ddcb))

# [2.1.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v2.0.0...v2.1.0) (2022-08-27)


### Features

* **sem-rel:** config ([8ef55ca](https://192.168.178.21/semantic-release/semantic-release-test/commit/8ef55ca20281ec5995c5f9b14fef6b2bf444a51b))
* **swagger:** Added Swagger Redirect ([9b30f50](https://192.168.178.21/semantic-release/semantic-release-test/commit/9b30f50af4e7eafe6db151cf68bf1596aee0812d))

# [2.0.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.8.0...v2.0.0) (2022-07-18)


### Features

* **h2:** Added H2 support ([7461e71](https://192.168.178.21/semantic-release/semantic-release-test/commit/7461e7187f17b509d85845b936cddf16375aacb3))


### BREAKING CHANGES

* **h2:** The UUID has been replaced with a Long

# [1.8.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.7.0...v1.8.0) (2022-07-18)


### Features

* **h2:** Added H2 support ([f5f4f0b](https://192.168.178.21/semantic-release/semantic-release-test/commit/f5f4f0ba7cfd9e69681fe9604e9bfa343a7f5838))

# [1.7.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.6.0...v1.7.0) (2022-07-18)


### Features

* **greeting:** Delete Greeting support ([342705d](https://192.168.178.21/semantic-release/semantic-release-test/commit/342705d745cf5e1b668f6cebd0dad867afde4a4f))

# [1.6.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.5.0...v1.6.0) (2022-07-17)


### Features

* **springdoc:** Added Springdoc OpenAPI Support ([512cce5](https://192.168.178.21/semantic-release/semantic-release-test/commit/512cce5419a3cf66729984ebf1847f1d12d640a4))

# [1.5.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.4.1...v1.5.0) (2022-07-15)


### Features

* **actuator:** Added Actuator Support ([6f4e0f6](https://192.168.178.21/semantic-release/semantic-release-test/commit/6f4e0f671a7ab987770ee9a69254bd1be81db17c))

## [1.4.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.4.0...v1.4.1) (2022-07-15)


### Bug Fixes

* **docker:** Java 17 ([082d489](https://192.168.178.21/semantic-release/semantic-release-test/commit/082d4893e9060b3296086f7dc5e7b342ed6d8bc4))

# [1.4.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.3.1...v1.4.0) (2022-07-15)


### Bug Fixes

* **semantic-release:** Config ([65c62bc](https://192.168.178.21/semantic-release/semantic-release-test/commit/65c62bc06094d76d635e71053adac6887056bbe9))
* **semantic-release:** Config ([0169263](https://192.168.178.21/semantic-release/semantic-release-test/commit/0169263bff7b4aaeb92514f71981807e6ed23702))
* **semantic-release:** Config ([985c215](https://192.168.178.21/semantic-release/semantic-release-test/commit/985c215553aaaf90164e5dc4c59ce0f960be5a5e))
* **semantic-release:** Config ([7193bd7](https://192.168.178.21/semantic-release/semantic-release-test/commit/7193bd783824564017c1d0fec41da51ee61a5800))


### Features

* **patch-greeting:** Patch Greeting endpoint ([6df15eb](https://192.168.178.21/semantic-release/semantic-release-test/commit/6df15eb20f0af41016703b76cfb1fcebd5311550))

## [1.3.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.3.0...v1.3.1) (2022-07-14)


### Bug Fixes

* **docker:** Dockerfile added ([660aaf7](https://192.168.178.21/semantic-release/semantic-release-test/commit/660aaf71bf795a8181887cd88241c12011cbd35b))
* **tekton:** tekton configuration ([f00f66c](https://192.168.178.21/semantic-release/semantic-release-test/commit/f00f66c9e809054feb089a453e7ecba08112110e))

# [1.3.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.2.0...v1.3.0) (2022-07-14)


### Bug Fixes

* **semantic-release:** new order branches ([b362153](https://192.168.178.21/semantic-release/semantic-release-test/commit/b362153dff909104e9af4c1f7952d54126341da2))
* **semantic-release:** new order branches ([390c1ab](https://192.168.178.21/semantic-release/semantic-release-test/commit/390c1ab8a876a9af5548e320b388c0d61b939cea))
* **semantic-release:** new order branches ([f31c138](https://192.168.178.21/semantic-release/semantic-release-test/commit/f31c1383fa03cd9ee44ccae7a6073906eff64792))
* **semantic-release:** new order branches ([1d92393](https://192.168.178.21/semantic-release/semantic-release-test/commit/1d923935edba64d7862e374fde8412a05e774339))


### Features

* **greeting:** update greeting ([5e7c5d1](https://192.168.178.21/semantic-release/semantic-release-test/commit/5e7c5d1e6c9802e32c3948a8b31b824c25773d5c))

# [1.3.0-snapshot.2](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.3.0-snapshot.1...v1.3.0-snapshot.2) (2022-07-14)


### Bug Fixes

* **semantic-release:** new order branches ([b362153](https://192.168.178.21/semantic-release/semantic-release-test/commit/b362153dff909104e9af4c1f7952d54126341da2))
* **semantic-release:** new order branches ([390c1ab](https://192.168.178.21/semantic-release/semantic-release-test/commit/390c1ab8a876a9af5548e320b388c0d61b939cea))

# [1.3.0-snapshot.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.2.0...v1.3.0-snapshot.1) (2022-07-14)


### Bug Fixes

* **semantic-release:** new order branches ([f31c138](https://192.168.178.21/semantic-release/semantic-release-test/commit/f31c1383fa03cd9ee44ccae7a6073906eff64792))
* **semantic-release:** new order branches ([1d92393](https://192.168.178.21/semantic-release/semantic-release-test/commit/1d923935edba64d7862e374fde8412a05e774339))


### Features

* **greeting:** update greeting ([5e7c5d1](https://192.168.178.21/semantic-release/semantic-release-test/commit/5e7c5d1e6c9802e32c3948a8b31b824c25773d5c))

# [1.2.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.1.1...v1.2.0) (2022-07-14)


### Features

* **semantic-release:** meerdere branches ([0f6d49a](https://192.168.178.21/semantic-release/semantic-release-test/commit/0f6d49ad5f8b4324c9aa39672fa354eb00a1fe34))

## [1.1.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.1.0...v1.1.1) (2022-07-14)


### Bug Fixes

* **semantic-release:** gitea plugin removed ([7b231b5](https://192.168.178.21/semantic-release/semantic-release-test/commit/7b231b5d4c3f33577de03d7d3b303f70a28b8466))

# [1.1.0](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.0.3...v1.1.0) (2022-07-14)


### Features

* **release:** nieuwe nep feature ([9360413](https://192.168.178.21/semantic-release/semantic-release-test/commit/9360413dd787678fc935bc8d889e67a398b4f1de))

## [1.0.3](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.0.2...v1.0.3) (2022-07-14)


### Bug Fixes

* **release:** probeersel met zelf gezette pom versie ([a99dfa2](https://192.168.178.21/semantic-release/semantic-release-test/commit/a99dfa262c97484269d2797a92abd9f891c611e3))

## [1.0.2](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.0.1...v1.0.2) (2022-07-14)


### Bug Fixes

* **release:** fixing the release config - exec ([731a264](https://192.168.178.21/semantic-release/semantic-release-test/commit/731a264d19659e40f240e2b734d84ff8a59f5da7))
* **release:** fixing the release config - maven ([64840d1](https://192.168.178.21/semantic-release/semantic-release-test/commit/64840d1e542b4cb8e0317d17da8a3a799816e8ed))
* **release:** fixing the release config - maven ([ef873e0](https://192.168.178.21/semantic-release/semantic-release-test/commit/ef873e0dd40d907acf21cd80a9c4502179f4b786))
* **release:** fixing the release config - maven ([b963b22](https://192.168.178.21/semantic-release/semantic-release-test/commit/b963b225b1791c53bf11cce13dfa62096b83538c))

## [1.0.1](https://192.168.178.21/semantic-release/semantic-release-test/compare/v1.0.0...v1.0.1) (2022-07-02)


### Bug Fixes

* **release:** fixing the release config - maven ([6f8cd3b](https://192.168.178.21/semantic-release/semantic-release-test/commit/6f8cd3b9531b69cff0246a0ddd2575370a70ea7c))

# 1.0.0 (2022-07-02)


### Bug Fixes

* **release:** fixing the release config ([3bb1b85](https://192.168.178.21/semantic-release/semantic-release-test/commit/3bb1b85632b8009ca2162e0c09f9a163765d5fcf))
* **release:** fixing the release config - ci mode ([49ceeec](https://192.168.178.21/semantic-release/semantic-release-test/commit/49ceeecd9e17df7b0e04688640a176e17908636c))
* **release:** fixing the release config - ci mode ([97144ae](https://192.168.178.21/semantic-release/semantic-release-test/commit/97144ae4f23e36f216de07fae8ede8c69d8fe124))
* **release:** fixing the release config - script ([d406c02](https://192.168.178.21/semantic-release/semantic-release-test/commit/d406c020f4d70b729560ba03065b41ce1a6a6f6c))


### Features

* **greeting:** added the greeting controller ([efeb768](https://192.168.178.21/semantic-release/semantic-release-test/commit/efeb76873c381ea99c7d4271857905b7e2a030c2))
* **greeting:** added the greeting controller ([f5ee316](https://192.168.178.21/semantic-release/semantic-release-test/commit/f5ee316febc29b57b68315097b9d0eeffd849c51))


### BREAKING CHANGES

* **greeting:** The old controller is deprecated
* **greeting:** The old controller is deprecated
