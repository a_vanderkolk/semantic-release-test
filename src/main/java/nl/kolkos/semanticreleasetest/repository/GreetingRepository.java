package nl.kolkos.semanticreleasetest.repository;

import nl.kolkos.semanticreleasetest.entity.Greeting;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface GreetingRepository extends PagingAndSortingRepository<Greeting, Long> {

    Optional<Greeting> findById(long id);

}
