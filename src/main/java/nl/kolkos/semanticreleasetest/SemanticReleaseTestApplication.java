package nl.kolkos.semanticreleasetest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemanticReleaseTestApplication {


	public static void main(String[] args) {
		SpringApplication.run(SemanticReleaseTestApplication.class, args);
	}

}
