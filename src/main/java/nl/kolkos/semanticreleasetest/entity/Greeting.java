package nl.kolkos.semanticreleasetest.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Setter(value = AccessLevel.PACKAGE)
@Getter
public class Greeting {

    @Id
    @JsonProperty(access=JsonProperty.Access.READ_ONLY)
    @GeneratedValue
    private long id;

    @NotNull
    private Date date;

    @NotNull
    private String greeting;
}
