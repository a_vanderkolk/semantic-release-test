package nl.kolkos.semanticreleasetest.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nl.kolkos.semanticreleasetest.entity.Greeting;
import nl.kolkos.semanticreleasetest.repository.GreetingRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;

@RequiredArgsConstructor
@Service
@Log4j2
public class GreetingService {

    private final GreetingRepository greetingRepository;

    public Greeting createNewGreeting(String greeting) {
        return Greeting.builder()
                .date(new Date())
                .greeting(greeting)
                .build();
    }

    public Greeting addGreeting(Greeting greeting) {
        return greetingRepository.save(greeting);
    }

    public Greeting getGreetingById(long id) {
        return greetingRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                        String.format("Cannot find greeting with id %d", id)));
    }

    public Page<Greeting> getGreetings() {
        Pageable sortByDateDesc =
                PageRequest.of(0, 100, Sort.by("date").descending());

        return greetingRepository.findAll(sortByDateDesc);
    }

    public void deleteGreeting(Greeting greeting) {
        log.info("deleting Greeting: {}", greeting);
        greetingRepository.delete(greeting);
    }

    public void deleteGreetingById(long id) {
        Greeting greeting = this.getGreetingById(id);
        this.deleteGreeting(greeting);
    }

}
