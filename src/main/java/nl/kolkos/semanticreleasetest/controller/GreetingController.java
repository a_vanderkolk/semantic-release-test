package nl.kolkos.semanticreleasetest.controller;

import lombok.RequiredArgsConstructor;
import nl.kolkos.semanticreleasetest.entity.Greeting;
import nl.kolkos.semanticreleasetest.service.GreetingService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class GreetingController {

    private final GreetingService greetingService;

    @PostMapping("/greetings")
    public Greeting addGreeting(@RequestBody @Valid Greeting greeting) {
        return greetingService.addGreeting(greeting);
    }

    @GetMapping("/greetings")
    public Page<Greeting> getGreetings() {
        return greetingService.getGreetings();
    }

    @GetMapping("/greetings/{id}")
    public Greeting getGreetingById(@PathVariable("id") long id){
        return greetingService.getGreetingById(id);
    }

    @PatchMapping("/greetings")
    public Greeting addGreetingWithMessage(String message) {
        Greeting greeting = greetingService.createNewGreeting(message);
        return greetingService.addGreeting(greeting);
    }

    @DeleteMapping("/greetings/{id}")
    public void deleteGreeting(@PathVariable("id") long id) {
        greetingService.deleteGreetingById(id);
    }
}
